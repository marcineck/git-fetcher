# Thetask

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

It works as webapp and also PWA (compile the code for production with `ng build --prod` and then you can run it locally with `serve -d dist/thetask` - just make sure you have the `serve` installed first 😎)

## PWA

Application can work as Progressive Web App, so can be easily installed on the phone (Android, iPhone) or even installed as Chrome app etc.

## Styling

Styles are placed in `assets/scss` folder.
Encapsulation for components is switched off.

If wish to use component styling with alongside `scss` simply comment the `encapsulation: ViewEncapsulation.None` and uncomment `styleUrls`.
  
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

If you need to call the author - have a look at the footer.

Cheers. 🍻
