import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  getRepositories(username) {
    return this.httpClient.get(`${environment.apiUrl}/users/${username}/repos`);
  }

  getBranches(callUrl) {
    return this.httpClient.get(callUrl);
  }
}
