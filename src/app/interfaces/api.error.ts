export interface ApiError {
  headers: {
    normalizedNames: {},
    lazyUpdate: boolean | string;
    headers: any
  };
  status: number;
  statusText: string;
  url: string;
  ok: boolean;
  name: string;
  error: {
    message: string;
    documentation_url: string;
  };
}
