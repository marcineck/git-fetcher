import {Subscription} from 'rxjs';

/**
 * Unsubscribe all subscriptions to avoid memory leaks
 * @param subscriptions
 */
export function unsubscribeAll(subscriptions: Array<Subscription>): void {
  subscriptions.forEach((sub) => {
    sub.unsubscribe();
  });
}

