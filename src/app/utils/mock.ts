import { UserRepository, UserRepositoryBranch } from '../interfaces/repository';

export const MOCKED_BRANCHES: Array<UserRepositoryBranch> = [
  {
    name: 'add-buildkite-filter-flags',
    commit: {
      sha: 'a9dd3ea751bc2fe2de58c0d524c1489afd00bdb5',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/a9dd3ea751bc2fe2de58c0d524c1489afd00bdb5'
    },
    protected: false
  },
  {
    name: 'build-optimizer',
    commit: {
      sha: '73e0756d7fc6ff3073bba56827e7ba3f7903146d',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/73e0756d7fc6ff3073bba56827e7ba3f7903146d'
    },
    protected: false
  },
  {
    name: 'cli',
    commit: {
      sha: '7b40f924131a01bf632140b745ac64330b9103dd',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/7b40f924131a01bf632140b745ac64330b9103dd'
    },
    protected: false
  },
  {
    name: 'closure_js_binary',
    commit: {
      sha: 'b7ca1eaf67107d3822027eac53cc45527f5c1f81',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/b7ca1eaf67107d3822027eac53cc45527f5c1f81'
    },
    protected: false
  },
  {
    name: 'closure_ts_binary',
    commit: {
      sha: '2e8fdc317e28ed704caee597b3f6aad6ef785629',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/2e8fdc317e28ed704caee597b3f6aad6ef785629'
    },
    protected: false
  },
  {
    name: 'content',
    commit: {
      sha: '8987fcbf692d380e302d76cf6b77faae31ee0a90',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/8987fcbf692d380e302d76cf6b77faae31ee0a90'
    },
    protected: false
  },
  {
    name: 'demo',
    commit: {
      sha: 'c84314554f05b91d93d00300e418e5ac477580ce',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/c84314554f05b91d93d00300e418e5ac477580ce'
    },
    protected: false
  },
  {
    name: 'deps_from_npm',
    commit: {
      sha: 'c1f9819a9cc5f431a970a4ab6125860eef32b5a0',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/c1f9819a9cc5f431a970a4ab6125860eef32b5a0'
    },
    protected: false
  },
  {
    name: 'devserver',
    commit: {
      sha: 'c52ea61f4606760034214893db95b55a6c2526e3',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/c52ea61f4606760034214893db95b55a6c2526e3'
    },
    protected: false
  },
  {
    name: 'faster_ng',
    commit: {
      sha: '93404aa4e532e333f9b9ecf2660980b877735ad7',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/93404aa4e532e333f9b9ecf2660980b877735ad7'
    },
    protected: false
  },
  {
    name: 'fewerscripts',
    commit: {
      sha: 'd40f738453637cf286310873e1bc7a9f17568b7b',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/d40f738453637cf286310873e1bc7a9f17568b7b'
    },
    protected: false
  },
  {
    name: 'grpc',
    commit: {
      sha: '0c1f05565021f220730c943559fa46ff77916b26',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/0c1f05565021f220730c943559fa46ff77916b26'
    },
    protected: false
  },
  {
    name: 'hacky_concat_serve',
    commit: {
      sha: 'faf1c6ad299e68ef4a86182461221c0133b0c1c1',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/faf1c6ad299e68ef4a86182461221c0133b0c1c1'
    },
    protected: false
  },
  {
    name: 'large-webpack',
    commit: {
      sha: '6586a9b4e6ab8bec93e59577d9ffe5f8db43d59b',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/6586a9b4e6ab8bec93e59577d9ffe5f8db43d59b'
    },
    protected: false
  },
  {
    name: 'large',
    commit: {
      sha: 'fa49cc403d249cc1cfcbd46c265d57b099971257',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/fa49cc403d249cc1cfcbd46c265d57b099971257'
    },
    protected: false
  },
  {
    name: 'managed_dirs',
    commit: {
      sha: 'b303620f5d6fa0706e4e4eb0eedb5d9c2b84f6f4',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/b303620f5d6fa0706e4e4eb0eedb5d9c2b84f6f4'
    },
    protected: false
  },
  {
    name: 'martyganz',
    commit: {
      sha: '5132b7be5af748a797b5c27bf508fecfe6031dd5',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/5132b7be5af748a797b5c27bf508fecfe6031dd5'
    },
    protected: false
  },
  {
    name: 'master',
    commit: {
      sha: '2f1abb9b027970ab9ff582dc40a558b42def8a2b',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/2f1abb9b027970ab9ff582dc40a558b42def8a2b'
    },
    protected: false
  },
  {
    name: 'medium-webpack',
    commit: {
      sha: '100faa2ece0be92d213844d87e46bebe0cda4ebb',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/100faa2ece0be92d213844d87e46bebe0cda4ebb'
    },
    protected: false
  },
  {
    name: 'medium',
    commit: {
      sha: 'dfd69cdfc6c3f57937fb3a59e4b25bb5412e0d7d',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/dfd69cdfc6c3f57937fb3a59e4b25bb5412e0d7d'
    },
    protected: false
  },
  {
    name: 'sass',
    commit: {
      sha: '3d41eaa11124c385831a7340ff28cf6351100117',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/3d41eaa11124c385831a7340ff28cf6351100117'
    },
    protected: false
  },
  {
    name: 'small-webpack',
    commit: {
      sha: '770170a86a493deeb10706a3c44ec5f891227042',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/770170a86a493deeb10706a3c44ec5f891227042'
    },
    protected: false
  },
  {
    name: 'ssr',
    commit: {
      sha: '75a853eca99d0c79800062100b3a36ad6b8520ec',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/75a853eca99d0c79800062100b3a36ad6b8520ec'
    },
    protected: false
  },
  {
    name: 'toolchains_version',
    commit: {
      sha: '6e101d9b1aeb7b939e2b057d216c7d30b9b617c5',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/6e101d9b1aeb7b939e2b057d216c7d30b9b617c5'
    },
    protected: false
  },
  {
    name: 'update_code_split',
    commit: {
      sha: 'f6cb03ef45dbfe587e3a5f82fb060c65fab85cbd',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/f6cb03ef45dbfe587e3a5f82fb060c65fab85cbd'
    },
    protected: false
  },
  {
    name: 'updt',
    commit: {
      sha: 'c8d916234cefaba69047354df788ad653b875b9e',
      url:
        'https://api.github.com/repos/angular/angular-bazel-example/commits/c8d916234cefaba69047354df788ad653b875b9e'
    },
    protected: false
  }
];

export const MOCKED_DATA: Array<UserRepository> = [
  {
    id: 172138943,
    node_id: 'MDEwOlJlcG9zaXRvcnkxNzIxMzg5NDM=',
    name: '.github',
    full_name: 'angular/.github',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/.github',
    description: 'default community health files',
    fork: false,
    url: 'https://api.github.com/repos/angular/.github',
    forks_url: 'https://api.github.com/repos/angular/.github/forks',
    keys_url: 'https://api.github.com/repos/angular/.github/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/.github/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/.github/teams',
    hooks_url: 'https://api.github.com/repos/angular/.github/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/.github/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/.github/events',
    assignees_url:
      'https://api.github.com/repos/angular/.github/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/.github/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/.github/tags',
    blobs_url: 'https://api.github.com/repos/angular/.github/git/blobs{/sha}',
    git_tags_url: 'https://api.github.com/repos/angular/.github/git/tags{/sha}',
    git_refs_url: 'https://api.github.com/repos/angular/.github/git/refs{/sha}',
    trees_url: 'https://api.github.com/repos/angular/.github/git/trees{/sha}',
    statuses_url: 'https://api.github.com/repos/angular/.github/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/.github/languages',
    stargazers_url: 'https://api.github.com/repos/angular/.github/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/.github/contributors',
    subscribers_url: 'https://api.github.com/repos/angular/.github/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/.github/subscription',
    commits_url: 'https://api.github.com/repos/angular/.github/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/.github/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/.github/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/.github/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/.github/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/.github/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/.github/merges',
    archive_url:
      'https://api.github.com/repos/angular/.github/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/.github/downloads',
    issues_url: 'https://api.github.com/repos/angular/.github/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/.github/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/.github/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/.github/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/.github/labels{/name}',
    releases_url: 'https://api.github.com/repos/angular/.github/releases{/id}',
    deployments_url: 'https://api.github.com/repos/angular/.github/deployments',
    created_at: '2019-02-22T21:45:58Z',
    updated_at: '2019-09-05T20:18:19Z',
    pushed_at: '2019-02-22T21:49:06Z',
    git_url: 'git://github.com/angular/.github.git',
    ssh_url: 'git@github.com:angular/.github.git',
    clone_url: 'https://github.com/angular/.github.git',
    svn_url: 'https://github.com/angular/.github',
    homepage: null,
    size: 0,
    stargazers_count: 1,
    watchers_count: 1,
    language: null,
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 3,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    forks: 3,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master'
  },
  {
    id: 35130077,
    node_id: 'MDEwOlJlcG9zaXRvcnkzNTEzMDA3Nw==',
    name: 'a',
    full_name: 'angular/a',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/a',
    description: 'Library for annotating ES5',
    fork: false,
    url: 'https://api.github.com/repos/angular/a',
    forks_url: 'https://api.github.com/repos/angular/a/forks',
    keys_url: 'https://api.github.com/repos/angular/a/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/a/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/a/teams',
    hooks_url: 'https://api.github.com/repos/angular/a/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/a/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/a/events',
    assignees_url: 'https://api.github.com/repos/angular/a/assignees{/user}',
    branches_url: 'https://api.github.com/repos/angular/a/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/a/tags',
    blobs_url: 'https://api.github.com/repos/angular/a/git/blobs{/sha}',
    git_tags_url: 'https://api.github.com/repos/angular/a/git/tags{/sha}',
    git_refs_url: 'https://api.github.com/repos/angular/a/git/refs{/sha}',
    trees_url: 'https://api.github.com/repos/angular/a/git/trees{/sha}',
    statuses_url: 'https://api.github.com/repos/angular/a/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/a/languages',
    stargazers_url: 'https://api.github.com/repos/angular/a/stargazers',
    contributors_url: 'https://api.github.com/repos/angular/a/contributors',
    subscribers_url: 'https://api.github.com/repos/angular/a/subscribers',
    subscription_url: 'https://api.github.com/repos/angular/a/subscription',
    commits_url: 'https://api.github.com/repos/angular/a/commits{/sha}',
    git_commits_url: 'https://api.github.com/repos/angular/a/git/commits{/sha}',
    comments_url: 'https://api.github.com/repos/angular/a/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/a/issues/comments{/number}',
    contents_url: 'https://api.github.com/repos/angular/a/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/a/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/a/merges',
    archive_url:
      'https://api.github.com/repos/angular/a/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/a/downloads',
    issues_url: 'https://api.github.com/repos/angular/a/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/a/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/a/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/a/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/a/labels{/name}',
    releases_url: 'https://api.github.com/repos/angular/a/releases{/id}',
    deployments_url: 'https://api.github.com/repos/angular/a/deployments',
    created_at: '2015-05-06T00:02:24Z',
    updated_at: '2020-03-20T02:23:30Z',
    pushed_at: '2018-04-12T18:05:36Z',
    git_url: 'git://github.com/angular/a.git',
    ssh_url: 'git@github.com:angular/a.git',
    clone_url: 'https://github.com/angular/a.git',
    svn_url: 'https://github.com/angular/a',
    homepage: null,
    size: 121,
    stargazers_count: 66,
    watchers_count: 66,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 29,
    mirror_url: null,
    archived: true,
    disabled: false,
    open_issues_count: 7,
    license: null,
    forks: 29,
    open_issues: 7,
    watchers: 66,
    default_branch: 'master'
  },
  {
    id: 24195339,
    node_id: 'MDEwOlJlcG9zaXRvcnkyNDE5NTMzOQ==',
    name: 'angular',
    full_name: 'angular/angular',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular',
    description: 'One framework. Mobile & desktop.',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular',
    forks_url: 'https://api.github.com/repos/angular/angular/forks',
    keys_url: 'https://api.github.com/repos/angular/angular/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular/tags',
    blobs_url: 'https://api.github.com/repos/angular/angular/git/blobs{/sha}',
    git_tags_url: 'https://api.github.com/repos/angular/angular/git/tags{/sha}',
    git_refs_url: 'https://api.github.com/repos/angular/angular/git/refs{/sha}',
    trees_url: 'https://api.github.com/repos/angular/angular/git/trees{/sha}',
    statuses_url: 'https://api.github.com/repos/angular/angular/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular/languages',
    stargazers_url: 'https://api.github.com/repos/angular/angular/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular/contributors',
    subscribers_url: 'https://api.github.com/repos/angular/angular/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular/subscription',
    commits_url: 'https://api.github.com/repos/angular/angular/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular/downloads',
    issues_url: 'https://api.github.com/repos/angular/angular/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/angular/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/angular/labels{/name}',
    releases_url: 'https://api.github.com/repos/angular/angular/releases{/id}',
    deployments_url: 'https://api.github.com/repos/angular/angular/deployments',
    created_at: '2014-09-18T16:12:01Z',
    updated_at: '2020-03-22T04:28:06Z',
    pushed_at: '2020-03-21T22:22:42Z',
    git_url: 'git://github.com/angular/angular.git',
    ssh_url: 'git@github.com:angular/angular.git',
    clone_url: 'https://github.com/angular/angular.git',
    svn_url: 'https://github.com/angular/angular',
    homepage: 'https://angular.io',
    size: 135927,
    stargazers_count: 59087,
    watchers_count: 59087,
    language: 'TypeScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: false,
    has_pages: false,
    forks_count: 16175,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 3323,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 16175,
    open_issues: 3323,
    watchers: 59087,
    default_branch: 'master'
  },
  {
    id: 97881987,
    node_id: 'MDEwOlJlcG9zaXRvcnk5Nzg4MTk4Nw==',
    name: 'angular-bazel-example',
    full_name: 'angular/angular-bazel-example',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-bazel-example',
    description: 'MOVED to the bazel nodejs monorepo 👉',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-bazel-example',
    forks_url:
      'https://api.github.com/repos/angular/angular-bazel-example/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-bazel-example/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-bazel-example/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-bazel-example/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-bazel-example/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-bazel-example/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-bazel-example/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-bazel-example/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-bazel-example/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-bazel-example/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-bazel-example/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-bazel-example/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-bazel-example/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-bazel-example/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-bazel-example/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-bazel-example/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-bazel-example/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-bazel-example/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-bazel-example/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-bazel-example/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-bazel-example/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-bazel-example/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-bazel-example/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-bazel-example/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-bazel-example/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-bazel-example/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-bazel-example/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-bazel-example/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-bazel-example/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-bazel-example/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-bazel-example/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-bazel-example/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-bazel-example/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-bazel-example/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-bazel-example/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-bazel-example/deployments',
    created_at: '2017-07-20T22:07:16Z',
    updated_at: '2020-02-14T16:43:50Z',
    pushed_at: '2019-09-12T22:21:57Z',
    git_url: 'git://github.com/angular/angular-bazel-example.git',
    ssh_url: 'git@github.com:angular/angular-bazel-example.git',
    clone_url: 'https://github.com/angular/angular-bazel-example.git',
    svn_url: 'https://github.com/angular/angular-bazel-example',
    homepage:
      'https://github.com/bazelbuild/rules_nodejs/tree/master/examples/angular',
    size: 3063,
    stargazers_count: 356,
    watchers_count: 356,
    language: 'TypeScript',
    has_issues: true,
    has_projects: false,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 93,
    mirror_url: null,
    archived: true,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 93,
    open_issues: 0,
    watchers: 356,
    default_branch: 'master'
  },
  {
    id: 9824425,
    node_id: 'MDEwOlJlcG9zaXRvcnk5ODI0NDI1',
    name: 'angular-carousel',
    full_name: 'angular/angular-carousel',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-carousel',
    description: 'ng-carousel directive',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-carousel',
    forks_url: 'https://api.github.com/repos/angular/angular-carousel/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-carousel/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-carousel/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-carousel/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-carousel/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-carousel/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-carousel/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-carousel/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-carousel/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-carousel/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-carousel/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-carousel/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-carousel/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-carousel/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-carousel/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-carousel/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-carousel/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-carousel/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-carousel/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-carousel/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-carousel/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-carousel/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-carousel/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-carousel/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-carousel/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-carousel/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-carousel/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-carousel/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-carousel/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-carousel/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-carousel/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-carousel/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-carousel/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-carousel/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-carousel/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-carousel/deployments',
    created_at: '2013-05-02T23:05:45Z',
    updated_at: '2020-02-09T06:26:54Z',
    pushed_at: '2013-12-02T20:18:46Z',
    git_url: 'git://github.com/angular/angular-carousel.git',
    ssh_url: 'git@github.com:angular/angular-carousel.git',
    clone_url: 'https://github.com/angular/angular-carousel.git',
    svn_url: 'https://github.com/angular/angular-carousel',
    homepage: null,
    size: 104,
    stargazers_count: 16,
    watchers_count: 16,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 11,
    mirror_url: null,
    archived: true,
    disabled: false,
    open_issues_count: 1,
    license: null,
    forks: 11,
    open_issues: 1,
    watchers: 16,
    default_branch: 'master'
  },
  {
    id: 36891867,
    node_id: 'MDEwOlJlcG9zaXRvcnkzNjg5MTg2Nw==',
    name: 'angular-cli',
    full_name: 'angular/angular-cli',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-cli',
    description: 'CLI tool for Angular',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-cli',
    forks_url: 'https://api.github.com/repos/angular/angular-cli/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-cli/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-cli/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-cli/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-cli/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-cli/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-cli/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-cli/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-cli/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-cli/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-cli/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-cli/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-cli/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-cli/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-cli/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-cli/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-cli/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-cli/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-cli/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-cli/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-cli/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-cli/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-cli/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-cli/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-cli/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-cli/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-cli/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-cli/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-cli/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-cli/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-cli/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-cli/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-cli/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-cli/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-cli/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-cli/deployments',
    created_at: '2015-06-04T19:49:37Z',
    updated_at: '2020-03-21T23:17:13Z',
    pushed_at: '2020-03-21T20:09:01Z',
    git_url: 'git://github.com/angular/angular-cli.git',
    ssh_url: 'git@github.com:angular/angular-cli.git',
    clone_url: 'https://github.com/angular/angular-cli.git',
    svn_url: 'https://github.com/angular/angular-cli',
    homepage: 'https://cli.angular.io/',
    size: 42515,
    stargazers_count: 23124,
    watchers_count: 23124,
    language: 'TypeScript',
    has_issues: true,
    has_projects: false,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 8670,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 646,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 8670,
    open_issues: 646,
    watchers: 23124,
    default_branch: 'master'
  },
  {
    id: 93683551,
    node_id: 'MDEwOlJlcG9zaXRvcnk5MzY4MzU1MQ==',
    name: 'angular-cli-stress-test',
    full_name: 'angular/angular-cli-stress-test',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-cli-stress-test',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-cli-stress-test',
    forks_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-cli-stress-test/deployments',
    created_at: '2017-06-07T22:04:10Z',
    updated_at: '2019-09-05T20:18:19Z',
    pushed_at: '2017-06-08T18:15:20Z',
    git_url: 'git://github.com/angular/angular-cli-stress-test.git',
    ssh_url: 'git@github.com:angular/angular-cli-stress-test.git',
    clone_url: 'https://github.com/angular/angular-cli-stress-test.git',
    svn_url: 'https://github.com/angular/angular-cli-stress-test',
    homepage: null,
    size: 1927,
    stargazers_count: 2,
    watchers_count: 2,
    language: 'TypeScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 2,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 2,
    open_issues: 0,
    watchers: 2,
    default_branch: 'master'
  },
  {
    id: 59776086,
    node_id: 'MDEwOlJlcG9zaXRvcnk1OTc3NjA4Ng==',
    name: 'angular-cn',
    full_name: 'angular/angular-cn',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-cn',
    description: 'Chinese localization of angular.io',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-cn',
    forks_url: 'https://api.github.com/repos/angular/angular-cn/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-cn/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-cn/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-cn/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-cn/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-cn/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-cn/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-cn/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-cn/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-cn/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-cn/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-cn/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-cn/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-cn/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-cn/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-cn/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-cn/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-cn/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-cn/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-cn/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-cn/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-cn/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-cn/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-cn/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-cn/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-cn/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-cn/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-cn/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-cn/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-cn/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/angular-cn/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-cn/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-cn/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/angular-cn/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-cn/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-cn/deployments',
    created_at: '2016-05-26T19:07:10Z',
    updated_at: '2020-03-16T12:21:21Z',
    pushed_at: '2020-03-07T12:40:07Z',
    git_url: 'git://github.com/angular/angular-cn.git',
    ssh_url: 'git@github.com:angular/angular-cn.git',
    clone_url: 'https://github.com/angular/angular-cn.git',
    svn_url: 'https://github.com/angular/angular-cn',
    homepage: null,
    size: 159707,
    stargazers_count: 725,
    watchers_count: 725,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 374,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 14,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 374,
    open_issues: 14,
    watchers: 725,
    default_branch: 'master'
  },
  {
    id: 9640159,
    node_id: 'MDEwOlJlcG9zaXRvcnk5NjQwMTU5',
    name: 'angular-component-spec',
    full_name: 'angular/angular-component-spec',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-component-spec',
    description: 'Specification for reusable AngularJS components',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-component-spec',
    forks_url:
      'https://api.github.com/repos/angular/angular-component-spec/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-component-spec/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-component-spec/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-component-spec/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-component-spec/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-component-spec/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-component-spec/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-component-spec/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-component-spec/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-component-spec/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-component-spec/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-component-spec/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-component-spec/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-component-spec/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-component-spec/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-component-spec/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-component-spec/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-component-spec/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-component-spec/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-component-spec/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-component-spec/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-component-spec/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-component-spec/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-component-spec/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-component-spec/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-component-spec/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-component-spec/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-component-spec/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-component-spec/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-component-spec/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-component-spec/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-component-spec/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-component-spec/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-component-spec/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-component-spec/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-component-spec/deployments',
    created_at: '2013-04-24T05:31:29Z',
    updated_at: '2020-02-09T06:23:26Z',
    pushed_at: '2015-05-20T13:43:48Z',
    git_url: 'git://github.com/angular/angular-component-spec.git',
    ssh_url: 'git@github.com:angular/angular-component-spec.git',
    clone_url: 'https://github.com/angular/angular-component-spec.git',
    svn_url: 'https://github.com/angular/angular-component-spec',
    homepage: null,
    size: 201,
    stargazers_count: 64,
    watchers_count: 64,
    language: null,
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 19,
    mirror_url: null,
    archived: true,
    disabled: false,
    open_issues_count: 2,
    license: null,
    forks: 19,
    open_issues: 2,
    watchers: 64,
    default_branch: 'master'
  },
  {
    id: 122383422,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMjIzODM0MjI=',
    name: 'angular-devkit-architect-builds',
    full_name: 'angular/angular-devkit-architect-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-devkit-architect-builds',
    description: 'Build artifacts for @angular-devkit/architect',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-devkit-architect-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-builds/deployments',
    created_at: '2018-02-21T19:33:07Z',
    updated_at: '2020-03-19T17:28:03Z',
    pushed_at: '2020-03-19T17:28:03Z',
    git_url: 'git://github.com/angular/angular-devkit-architect-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-architect-builds.git',
    clone_url: 'https://github.com/angular/angular-devkit-architect-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-architect-builds',
    homepage: '',
    size: 1476,
    stargazers_count: 1,
    watchers_count: 1,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 2,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 2,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master'
  },
  {
    id: 122383478,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMjIzODM0Nzg=',
    name: 'angular-devkit-architect-cli-builds',
    full_name: 'angular/angular-devkit-architect-cli-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-devkit-architect-cli-builds',
    description: 'Build artifacts for @angular-devkit/architect-cli',
    fork: false,
    url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-architect-cli-builds/deployments',
    created_at: '2018-02-21T19:33:36Z',
    updated_at: '2020-03-19T17:28:09Z',
    pushed_at: '2020-03-19T17:28:10Z',
    git_url: 'git://github.com/angular/angular-devkit-architect-cli-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-architect-cli-builds.git',
    clone_url:
      'https://github.com/angular/angular-devkit-architect-cli-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-architect-cli-builds',
    homepage: '',
    size: 1302,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 2,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 2,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master'
  },
  {
    id: 127210390,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMjcyMTAzOTA=',
    name: 'angular-devkit-build-angular-builds',
    full_name: 'angular/angular-devkit-build-angular-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-devkit-build-angular-builds',
    description: 'Build artifacts for @angular-devkit/build-angular',
    fork: false,
    url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-angular-builds/deployments',
    created_at: '2018-03-28T23:29:21Z',
    updated_at: '2020-03-19T17:28:17Z',
    pushed_at: '2020-03-19T17:28:17Z',
    git_url: 'git://github.com/angular/angular-devkit-build-angular-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-build-angular-builds.git',
    clone_url:
      'https://github.com/angular/angular-devkit-build-angular-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-build-angular-builds',
    homepage: '',
    size: 3779,
    stargazers_count: 7,
    watchers_count: 7,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 8,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 3,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 8,
    open_issues: 3,
    watchers: 7,
    default_branch: 'master'
  },
  {
    id: 127200806,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMjcyMDA4MDY=',
    name: 'angular-devkit-build-ng-packagr-builds',
    full_name: 'angular/angular-devkit-build-ng-packagr-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url:
      'https://github.com/angular/angular-devkit-build-ng-packagr-builds',
    description: 'Build artifacts for @angular-devkit/build-ng-packagr',
    fork: false,
    url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-ng-packagr-builds/deployments',
    created_at: '2018-03-28T21:29:54Z',
    updated_at: '2020-03-19T17:28:23Z',
    pushed_at: '2020-03-19T17:28:24Z',
    git_url:
      'git://github.com/angular/angular-devkit-build-ng-packagr-builds.git',
    ssh_url:
      'git@github.com:angular/angular-devkit-build-ng-packagr-builds.git',
    clone_url:
      'https://github.com/angular/angular-devkit-build-ng-packagr-builds.git',
    svn_url:
      'https://github.com/angular/angular-devkit-build-ng-packagr-builds',
    homepage: '',
    size: 1182,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 1,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 1,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master'
  },
  {
    id: 119606080,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMTk2MDYwODA=',
    name: 'angular-devkit-build-optimizer-builds',
    full_name: 'angular/angular-devkit-build-optimizer-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url:
      'https://github.com/angular/angular-devkit-build-optimizer-builds',
    description: 'Build artifacts for @angular-devkit/build-optimizer',
    fork: false,
    url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-optimizer-builds/deployments',
    created_at: '2018-01-30T23:08:32Z',
    updated_at: '2020-03-19T17:28:30Z',
    pushed_at: '2020-03-19T17:28:31Z',
    git_url:
      'git://github.com/angular/angular-devkit-build-optimizer-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-build-optimizer-builds.git',
    clone_url:
      'https://github.com/angular/angular-devkit-build-optimizer-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-build-optimizer-builds',
    homepage: '',
    size: 1514,
    stargazers_count: 1,
    watchers_count: 1,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 2,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 2,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master'
  },
  {
    id: 122398295,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMjIzOTgyOTU=',
    name: 'angular-devkit-build-webpack-builds',
    full_name: 'angular/angular-devkit-build-webpack-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-devkit-build-webpack-builds',
    description: 'Build artifacts for @angular-devkit/build-webpack',
    fork: false,
    url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-build-webpack-builds/deployments',
    created_at: '2018-02-21T21:50:59Z',
    updated_at: '2020-03-19T17:28:36Z',
    pushed_at: '2020-03-19T17:28:37Z',
    git_url: 'git://github.com/angular/angular-devkit-build-webpack-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-build-webpack-builds.git',
    clone_url:
      'https://github.com/angular/angular-devkit-build-webpack-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-build-webpack-builds',
    homepage: '',
    size: 1651,
    stargazers_count: 3,
    watchers_count: 3,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 2,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 2,
    open_issues: 0,
    watchers: 3,
    default_branch: 'master'
  },
  {
    id: 118971226,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMTg5NzEyMjY=',
    name: 'angular-devkit-core-builds',
    full_name: 'angular/angular-devkit-core-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-devkit-core-builds',
    description: 'Snapshots for @angular-devkit/core',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-devkit-core-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-core-builds/deployments',
    created_at: '2018-01-25T21:44:11Z',
    updated_at: '2020-03-19T17:28:43Z',
    pushed_at: '2020-03-19T17:28:44Z',
    git_url: 'git://github.com/angular/angular-devkit-core-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-core-builds.git',
    clone_url: 'https://github.com/angular/angular-devkit-core-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-core-builds',
    homepage: null,
    size: 2448,
    stargazers_count: 1,
    watchers_count: 1,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 1,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 1,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master'
  },
  {
    id: 118981589,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMTg5ODE1ODk=',
    name: 'angular-devkit-schematics-builds',
    full_name: 'angular/angular-devkit-schematics-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-devkit-schematics-builds',
    description: 'Build artifacts for @angular-devkit/schematics',
    fork: false,
    url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-builds/deployments',
    created_at: '2018-01-25T23:53:22Z',
    updated_at: '2020-03-19T17:28:50Z',
    pushed_at: '2020-03-19T17:28:51Z',
    git_url: 'git://github.com/angular/angular-devkit-schematics-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-schematics-builds.git',
    clone_url:
      'https://github.com/angular/angular-devkit-schematics-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-schematics-builds',
    homepage: '',
    size: 2307,
    stargazers_count: 11,
    watchers_count: 11,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 2,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 1,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 2,
    open_issues: 1,
    watchers: 11,
    default_branch: 'master'
  },
  {
    id: 119606006,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMTk2MDYwMDY=',
    name: 'angular-devkit-schematics-cli-builds',
    full_name: 'angular/angular-devkit-schematics-cli-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-devkit-schematics-cli-builds',
    description: 'Build artifacts for @angular-devkit/schematics-cli',
    fork: false,
    url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds',
    forks_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/teams',
    hooks_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-devkit-schematics-cli-builds/deployments',
    created_at: '2018-01-30T23:07:54Z',
    updated_at: '2020-03-19T17:28:57Z',
    pushed_at: '2020-03-19T17:28:58Z',
    git_url:
      'git://github.com/angular/angular-devkit-schematics-cli-builds.git',
    ssh_url: 'git@github.com:angular/angular-devkit-schematics-cli-builds.git',
    clone_url:
      'https://github.com/angular/angular-devkit-schematics-cli-builds.git',
    svn_url: 'https://github.com/angular/angular-devkit-schematics-cli-builds',
    homepage: '',
    size: 1402,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 1,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 1,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master'
  },
  {
    id: 51412029,
    node_id: 'MDEwOlJlcG9zaXRvcnk1MTQxMjAyOQ==',
    name: 'angular-electron',
    full_name: 'angular/angular-electron',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-electron',
    description: 'Angular2 + Electron',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-electron',
    forks_url: 'https://api.github.com/repos/angular/angular-electron/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-electron/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-electron/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-electron/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-electron/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-electron/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-electron/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-electron/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-electron/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-electron/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-electron/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-electron/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-electron/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-electron/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-electron/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-electron/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-electron/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-electron/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-electron/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-electron/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-electron/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-electron/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-electron/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-electron/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-electron/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-electron/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-electron/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-electron/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-electron/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-electron/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-electron/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-electron/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-electron/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-electron/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-electron/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-electron/deployments',
    created_at: '2016-02-10T00:35:35Z',
    updated_at: '2020-02-14T17:04:07Z',
    pushed_at: '2018-04-19T07:24:48Z',
    git_url: 'git://github.com/angular/angular-electron.git',
    ssh_url: 'git@github.com:angular/angular-electron.git',
    clone_url: 'https://github.com/angular/angular-electron.git',
    svn_url: 'https://github.com/angular/angular-electron',
    homepage: null,
    size: 73,
    stargazers_count: 630,
    watchers_count: 630,
    language: 'TypeScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 129,
    mirror_url: null,
    archived: true,
    disabled: false,
    open_issues_count: 23,
    license: null,
    forks: 129,
    open_issues: 23,
    watchers: 630,
    default_branch: 'master'
  },
  {
    id: 63199840,
    node_id: 'MDEwOlJlcG9zaXRvcnk2MzE5OTg0MA==',
    name: 'angular-es',
    full_name: 'angular/angular-es',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-es',
    description: 'Localization of angular.io to Spanish',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-es',
    forks_url: 'https://api.github.com/repos/angular/angular-es/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-es/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-es/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-es/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-es/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-es/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-es/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-es/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-es/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-es/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-es/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-es/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-es/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-es/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-es/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-es/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-es/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-es/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-es/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-es/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-es/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-es/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-es/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-es/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-es/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-es/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-es/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-es/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-es/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-es/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/angular-es/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-es/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-es/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/angular-es/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-es/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-es/deployments',
    created_at: '2016-07-12T23:46:44Z',
    updated_at: '2019-09-05T20:18:19Z',
    pushed_at: '2018-10-18T23:20:45Z',
    git_url: 'git://github.com/angular/angular-es.git',
    ssh_url: 'git@github.com:angular/angular-es.git',
    clone_url: 'https://github.com/angular/angular-es.git',
    svn_url: 'https://github.com/angular/angular-es',
    homepage: null,
    size: 32143,
    stargazers_count: 14,
    watchers_count: 14,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 26,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 56,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 26,
    open_issues: 56,
    watchers: 14,
    default_branch: 'master'
  },
  {
    id: 63256516,
    node_id: 'MDEwOlJlcG9zaXRvcnk2MzI1NjUxNg==',
    name: 'angular-fr',
    full_name: 'angular/angular-fr',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-fr',
    description: 'French localization for angular.io',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-fr',
    forks_url: 'https://api.github.com/repos/angular/angular-fr/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-fr/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-fr/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-fr/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-fr/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-fr/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-fr/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-fr/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-fr/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-fr/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-fr/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-fr/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-fr/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-fr/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-fr/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-fr/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-fr/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-fr/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-fr/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-fr/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-fr/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-fr/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-fr/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-fr/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-fr/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-fr/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-fr/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-fr/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-fr/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-fr/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/angular-fr/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-fr/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-fr/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/angular-fr/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-fr/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-fr/deployments',
    created_at: '2016-07-13T15:11:43Z',
    updated_at: '2020-02-15T18:50:57Z',
    pushed_at: '2016-10-30T10:29:25Z',
    git_url: 'git://github.com/angular/angular-fr.git',
    ssh_url: 'git@github.com:angular/angular-fr.git',
    clone_url: 'https://github.com/angular/angular-fr.git',
    svn_url: 'https://github.com/angular/angular-fr',
    homepage: null,
    size: 27064,
    stargazers_count: 13,
    watchers_count: 13,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 22,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 23,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 22,
    open_issues: 23,
    watchers: 13,
    default_branch: 'master'
  },
  {
    id: 21144872,
    node_id: 'MDEwOlJlcG9zaXRvcnkyMTE0NDg3Mg==',
    name: 'angular-hint',
    full_name: 'angular/angular-hint',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-hint',
    description: 'run-time hinting for AngularJS applications',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-hint',
    forks_url: 'https://api.github.com/repos/angular/angular-hint/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-hint/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-hint/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-hint/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-hint/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-hint/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-hint/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-hint/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-hint/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-hint/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-hint/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-hint/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-hint/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-hint/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-hint/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-hint/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-hint/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-hint/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-hint/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-hint/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-hint/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-hint/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-hint/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-hint/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-hint/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-hint/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-hint/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-hint/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-hint/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-hint/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-hint/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-hint/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-hint/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-hint/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-hint/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-hint/deployments',
    created_at: '2014-06-23T22:46:16Z',
    updated_at: '2020-03-05T17:07:31Z',
    pushed_at: '2017-11-22T15:44:08Z',
    git_url: 'git://github.com/angular/angular-hint.git',
    ssh_url: 'git@github.com:angular/angular-hint.git',
    clone_url: 'https://github.com/angular/angular-hint.git',
    svn_url: 'https://github.com/angular/angular-hint',
    homepage: '',
    size: 277,
    stargazers_count: 375,
    watchers_count: 375,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: false,
    has_pages: false,
    forks_count: 49,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 29,
    license: null,
    forks: 49,
    open_issues: 29,
    watchers: 375,
    default_branch: 'master'
  },
  {
    id: 65341357,
    node_id: 'MDEwOlJlcG9zaXRvcnk2NTM0MTM1Nw==',
    name: 'angular-ja',
    full_name: 'angular/angular-ja',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-ja',
    description: 'repository for Japanese localization of angular.io',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-ja',
    forks_url: 'https://api.github.com/repos/angular/angular-ja/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-ja/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-ja/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-ja/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-ja/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-ja/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-ja/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-ja/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-ja/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-ja/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-ja/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-ja/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-ja/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-ja/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-ja/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-ja/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-ja/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-ja/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-ja/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-ja/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-ja/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-ja/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-ja/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-ja/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-ja/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-ja/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-ja/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-ja/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-ja/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-ja/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/angular-ja/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-ja/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-ja/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/angular-ja/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-ja/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-ja/deployments',
    created_at: '2016-08-10T01:49:04Z',
    updated_at: '2020-03-20T06:58:41Z',
    pushed_at: '2020-03-20T09:52:30Z',
    git_url: 'git://github.com/angular/angular-ja.git',
    ssh_url: 'git@github.com:angular/angular-ja.git',
    clone_url: 'https://github.com/angular/angular-ja.git',
    svn_url: 'https://github.com/angular/angular-ja',
    homepage: 'https://angular.jp',
    size: 39336,
    stargazers_count: 174,
    watchers_count: 174,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: false,
    has_pages: false,
    forks_count: 100,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 18,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 100,
    open_issues: 18,
    watchers: 174,
    default_branch: 'master'
  },
  {
    id: 1256400,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMjU2NDAw',
    name: 'angular-jquery-ui',
    full_name: 'angular/angular-jquery-ui',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-jquery-ui',
    description: 'jQueryUI widgets wrapped as angular widgets',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-jquery-ui',
    forks_url: 'https://api.github.com/repos/angular/angular-jquery-ui/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-jquery-ui/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-jquery-ui/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-jquery-ui/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-jquery-ui/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-jquery-ui/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-jquery-ui/deployments',
    created_at: '2011-01-14T23:53:36Z',
    updated_at: '2020-02-09T06:27:16Z',
    pushed_at: '2011-11-15T04:55:59Z',
    git_url: 'git://github.com/angular/angular-jquery-ui.git',
    ssh_url: 'git@github.com:angular/angular-jquery-ui.git',
    clone_url: 'https://github.com/angular/angular-jquery-ui.git',
    svn_url: 'https://github.com/angular/angular-jquery-ui',
    homepage: '',
    size: 4086,
    stargazers_count: 90,
    watchers_count: 90,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 30,
    mirror_url: null,
    archived: true,
    disabled: false,
    open_issues_count: 3,
    license: null,
    forks: 30,
    open_issues: 3,
    watchers: 90,
    default_branch: 'master'
  },
  {
    id: 65340369,
    node_id: 'MDEwOlJlcG9zaXRvcnk2NTM0MDM2OQ==',
    name: 'angular-ko',
    full_name: 'angular/angular-ko',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-ko',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-ko',
    forks_url: 'https://api.github.com/repos/angular/angular-ko/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-ko/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-ko/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-ko/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-ko/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-ko/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-ko/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-ko/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-ko/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-ko/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-ko/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-ko/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-ko/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-ko/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-ko/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-ko/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-ko/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-ko/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-ko/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-ko/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-ko/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-ko/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-ko/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-ko/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-ko/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-ko/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-ko/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-ko/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-ko/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-ko/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/angular-ko/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-ko/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-ko/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/angular-ko/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-ko/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-ko/deployments',
    created_at: '2016-08-10T01:30:37Z',
    updated_at: '2019-11-27T05:25:59Z',
    pushed_at: '2017-06-23T13:49:25Z',
    git_url: 'git://github.com/angular/angular-ko.git',
    ssh_url: 'git@github.com:angular/angular-ko.git',
    clone_url: 'https://github.com/angular/angular-ko.git',
    svn_url: 'https://github.com/angular/angular-ko',
    homepage: null,
    size: 36984,
    stargazers_count: 34,
    watchers_count: 34,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 21,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 11,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 21,
    open_issues: 11,
    watchers: 34,
    default_branch: 'master'
  },
  {
    id: 1452079,
    node_id: 'MDEwOlJlcG9zaXRvcnkxNDUyMDc5',
    name: 'angular-phonecat',
    full_name: 'angular/angular-phonecat',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-phonecat',
    description: 'Tutorial on building an angular application.',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-phonecat',
    forks_url: 'https://api.github.com/repos/angular/angular-phonecat/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-phonecat/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-phonecat/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-phonecat/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-phonecat/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-phonecat/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-phonecat/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-phonecat/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-phonecat/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-phonecat/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-phonecat/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-phonecat/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-phonecat/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-phonecat/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-phonecat/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-phonecat/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-phonecat/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-phonecat/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-phonecat/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-phonecat/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-phonecat/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-phonecat/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-phonecat/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-phonecat/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-phonecat/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-phonecat/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-phonecat/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-phonecat/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-phonecat/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-phonecat/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-phonecat/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-phonecat/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-phonecat/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-phonecat/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-phonecat/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-phonecat/deployments',
    created_at: '2011-03-07T21:42:29Z',
    updated_at: '2020-03-16T13:45:47Z',
    pushed_at: '2020-02-14T02:48:28Z',
    git_url: 'git://github.com/angular/angular-phonecat.git',
    ssh_url: 'git@github.com:angular/angular-phonecat.git',
    clone_url: 'https://github.com/angular/angular-phonecat.git',
    svn_url: 'https://github.com/angular/angular-phonecat',
    homepage: 'http://docs.angularjs.org/tutorial',
    size: 103298,
    stargazers_count: 3097,
    watchers_count: 3097,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: true,
    forks_count: 4858,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 5,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 4858,
    open_issues: 5,
    watchers: 3097,
    default_branch: 'master'
  },
  {
    id: 11749871,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMTc0OTg3MQ==',
    name: 'angular-prs',
    full_name: 'angular/angular-prs',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-prs',
    description:
      'app to visualize PR queue activity for the angular.js project. currently the data is stored in the repo because of github query rate limits.',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-prs',
    forks_url: 'https://api.github.com/repos/angular/angular-prs/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-prs/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-prs/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-prs/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-prs/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-prs/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-prs/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-prs/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-prs/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-prs/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-prs/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-prs/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-prs/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-prs/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-prs/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-prs/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-prs/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-prs/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-prs/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-prs/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-prs/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-prs/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-prs/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-prs/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-prs/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-prs/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-prs/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-prs/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-prs/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-prs/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-prs/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-prs/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-prs/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-prs/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-prs/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-prs/deployments',
    created_at: '2013-07-29T21:10:01Z',
    updated_at: '2019-09-05T20:18:18Z',
    pushed_at: '2015-02-27T20:53:21Z',
    git_url: 'git://github.com/angular/angular-prs.git',
    ssh_url: 'git@github.com:angular/angular-prs.git',
    clone_url: 'https://github.com/angular/angular-prs.git',
    svn_url: 'https://github.com/angular/angular-prs',
    homepage: 'http://angular.github.io/angular-prs/',
    size: 21587,
    stargazers_count: 6,
    watchers_count: 6,
    language: 'ApacheConf',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: true,
    forks_count: 3,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 1,
    license: null,
    forks: 3,
    open_issues: 1,
    watchers: 6,
    default_branch: 'master'
  },
  {
    id: 63377116,
    node_id: 'MDEwOlJlcG9zaXRvcnk2MzM3NzExNg==',
    name: 'angular-pt',
    full_name: 'angular/angular-pt',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-pt',
    description: 'Localization of angular.io for pt-BR',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-pt',
    forks_url: 'https://api.github.com/repos/angular/angular-pt/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-pt/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-pt/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-pt/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-pt/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-pt/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-pt/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-pt/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-pt/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-pt/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-pt/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-pt/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-pt/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-pt/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-pt/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/angular/angular-pt/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-pt/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-pt/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-pt/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-pt/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-pt/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-pt/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-pt/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-pt/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-pt/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-pt/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-pt/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-pt/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/angular/angular-pt/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-pt/issues{/number}',
    pulls_url: 'https://api.github.com/repos/angular/angular-pt/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-pt/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-pt/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/angular/angular-pt/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-pt/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-pt/deployments',
    created_at: '2016-07-14T23:47:09Z',
    updated_at: '2019-10-01T20:48:43Z',
    pushed_at: '2016-08-31T15:59:11Z',
    git_url: 'git://github.com/angular/angular-pt.git',
    ssh_url: 'git@github.com:angular/angular-pt.git',
    clone_url: 'https://github.com/angular/angular-pt.git',
    svn_url: 'https://github.com/angular/angular-pt',
    homepage: null,
    size: 27063,
    stargazers_count: 17,
    watchers_count: 17,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 14,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 33,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 14,
    open_issues: 33,
    watchers: 17,
    default_branch: 'master'
  },
  {
    id: 127208294,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMjcyMDgyOTQ=',
    name: 'angular-pwa-builds',
    full_name: 'angular/angular-pwa-builds',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-pwa-builds',
    description: 'Build artifacts for @angular/pwa',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-pwa-builds',
    forks_url: 'https://api.github.com/repos/angular/angular-pwa-builds/forks',
    keys_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-pwa-builds/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-pwa-builds/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-pwa-builds/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-pwa-builds/deployments',
    created_at: '2018-03-28T22:58:38Z',
    updated_at: '2020-03-19T17:27:56Z',
    pushed_at: '2020-03-19T17:27:57Z',
    git_url: 'git://github.com/angular/angular-pwa-builds.git',
    ssh_url: 'git@github.com:angular/angular-pwa-builds.git',
    clone_url: 'https://github.com/angular/angular-pwa-builds.git',
    svn_url: 'https://github.com/angular/angular-pwa-builds',
    homepage: '',
    size: 1276,
    stargazers_count: 6,
    watchers_count: 6,
    language: 'JavaScript',
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 3,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 3,
    open_issues: 0,
    watchers: 6,
    default_branch: 'master'
  },
  {
    id: 1195004,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMTk1MDA0',
    name: 'angular-seed',
    full_name: 'angular/angular-seed',
    private: false,
    owner: {
      login: 'angular',
      id: 139426,
      node_id: 'MDEyOk9yZ2FuaXphdGlvbjEzOTQyNg==',
      avatar_url: 'https://avatars3.githubusercontent.com/u/139426?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/angular',
      html_url: 'https://github.com/angular',
      followers_url: 'https://api.github.com/users/angular/followers',
      following_url:
        'https://api.github.com/users/angular/following{/other_user}',
      gists_url: 'https://api.github.com/users/angular/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/angular/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/angular/subscriptions',
      organizations_url: 'https://api.github.com/users/angular/orgs',
      repos_url: 'https://api.github.com/users/angular/repos',
      events_url: 'https://api.github.com/users/angular/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/angular/received_events',
      type: 'Organization',
      site_admin: false
    },
    html_url: 'https://github.com/angular/angular-seed',
    description: 'Seed project for angular apps. ',
    fork: false,
    url: 'https://api.github.com/repos/angular/angular-seed',
    forks_url: 'https://api.github.com/repos/angular/angular-seed/forks',
    keys_url: 'https://api.github.com/repos/angular/angular-seed/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/angular/angular-seed/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/angular/angular-seed/teams',
    hooks_url: 'https://api.github.com/repos/angular/angular-seed/hooks',
    issue_events_url:
      'https://api.github.com/repos/angular/angular-seed/issues/events{/number}',
    events_url: 'https://api.github.com/repos/angular/angular-seed/events',
    assignees_url:
      'https://api.github.com/repos/angular/angular-seed/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/angular/angular-seed/branches{/branch}',
    tags_url: 'https://api.github.com/repos/angular/angular-seed/tags',
    blobs_url:
      'https://api.github.com/repos/angular/angular-seed/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/angular/angular-seed/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/angular/angular-seed/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/angular/angular-seed/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/angular/angular-seed/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/angular/angular-seed/languages',
    stargazers_url:
      'https://api.github.com/repos/angular/angular-seed/stargazers',
    contributors_url:
      'https://api.github.com/repos/angular/angular-seed/contributors',
    subscribers_url:
      'https://api.github.com/repos/angular/angular-seed/subscribers',
    subscription_url:
      'https://api.github.com/repos/angular/angular-seed/subscription',
    commits_url:
      'https://api.github.com/repos/angular/angular-seed/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/angular/angular-seed/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/angular/angular-seed/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/angular/angular-seed/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/angular/angular-seed/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/angular/angular-seed/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/angular/angular-seed/merges',
    archive_url:
      'https://api.github.com/repos/angular/angular-seed/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/angular/angular-seed/downloads',
    issues_url:
      'https://api.github.com/repos/angular/angular-seed/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/angular/angular-seed/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/angular/angular-seed/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/angular/angular-seed/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/angular/angular-seed/labels{/name}',
    releases_url:
      'https://api.github.com/repos/angular/angular-seed/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/angular/angular-seed/deployments',
    created_at: '2010-12-24T06:07:50Z',
    updated_at: '2020-03-21T22:11:17Z',
    pushed_at: '2020-01-26T10:07:41Z',
    git_url: 'git://github.com/angular/angular-seed.git',
    ssh_url: 'git@github.com:angular/angular-seed.git',
    clone_url: 'https://github.com/angular/angular-seed.git',
    svn_url: 'https://github.com/angular/angular-seed',
    homepage: 'http://angularjs.org/',
    size: 14494,
    stargazers_count: 13350,
    watchers_count: 13350,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 7321,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 19,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 7321,
    open_issues: 19,
    watchers: 13350,
    default_branch: 'master'
  }
];
