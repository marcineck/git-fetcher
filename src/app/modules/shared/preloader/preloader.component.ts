import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PreloaderComponent {
  @Input() isSmall = false;
}
