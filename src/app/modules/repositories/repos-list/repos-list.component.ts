import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserRepository} from '../../../interfaces/repository';
import {environment} from '../../../../environments/environment';
import {ApiService} from '../../../services/api.service';
import {Subscription} from 'rxjs';
import {ApiError} from '../../../interfaces/api.error';
import {unsubscribeAll} from '../../../utils/utils';

@Component({
  selector: 'app-repos-list',
  templateUrl: './repos-list.component.html',
  encapsulation: ViewEncapsulation.None
  // styleUrls: ['./repos-list.component.scss'],
})
export class ReposListComponent implements OnDestroy {

  subscriptions: Array<Subscription> = [];
  repositories: Array<UserRepository> = [];
  // In case API gets into trouble
  errorData: ApiError = null;
  // Preloader status
  isLoading = false;
  // Keep the username so we can render it elsewhere
  username = null;
  // Branch Counter Keeper - to avoid messing up the interfaces
  branchCountKeeper: Array<number> = [];

  constructor(private route: ActivatedRoute, private api: ApiService) {
    this.subscriptions.push(
      this.route.params.subscribe((routeParams: { username?: string }) => {
        console.log(routeParams);
        if (routeParams.hasOwnProperty('username')) {
          this.username = routeParams.username;
          this.fetchRepositories();
          // this.filterRepositories();
        }
      }));
  }

  filterRepositories(repositories) {
    return repositories.filter((el) => {
      return !el.fork;
    });
  }

  fetchRepositories() {
    // Reset errors
    this.errorData = null;
    this.isLoading = true;
    this.repositories = [];

    this.subscriptions.push(
      this.api.getRepositories(this.username)
        .subscribe((apiData: Array<UserRepository>) => {
          this.repositories = this.filterRepositories(apiData);
          this.isLoading = false;
        }, (apiError: ApiError) => {
          this.errorData = apiError;
          this.isLoading = false;
        })
    );
  }

  constructBranchUrl(repository: UserRepository) {
    return `${environment.apiUrl}/repos/${repository.owner.login}/${repository.name}/branches`;
  }

  ngOnDestroy(): void {
    unsubscribeAll(this.subscriptions);
  }

}
