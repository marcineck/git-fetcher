import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReposBranchListComponent } from './repos-branch-list.component';

describe('ReposBranchListComponent', () => {
  let component: ReposBranchListComponent;
  let fixture: ComponentFixture<ReposBranchListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReposBranchListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReposBranchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
