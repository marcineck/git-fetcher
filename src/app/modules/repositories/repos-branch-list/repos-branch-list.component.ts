import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {UserRepositoryBranch} from '../../../interfaces/repository';
import {ApiService} from '../../../services/api.service';
import {ApiError} from '../../../interfaces/api.error';
import {unsubscribeAll} from '../../../utils/utils';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-repos-branch-list',
  templateUrl: './repos-branch-list.component.html',
  encapsulation: ViewEncapsulation.None
  // styleUrls: ['./repos-branch-list.component.scss']
})
export class ReposBranchListComponent implements OnInit, OnDestroy {
  branches: Array<UserRepositoryBranch> = [];
  errorData: ApiError = null;
  subscriptions: Array<Subscription> = [];
  @Input() branchUrl;
  @Output() branchesCount = new EventEmitter();
  // Preloader status
  isLoading = false;

  constructor(private api: ApiService) {
  }

  ngOnInit() {
    // Input data is available after the component is initiated - not in the constructor
    this.getBranches();
  }

  getBranches() {
    this.isLoading = true;
    // It would be smart to add some caching, but it was not the part of the task
    this.subscriptions.push(
      this.api.getBranches(this.branchUrl)
        .subscribe((branchApiResponse: any) => {
          this.branches = branchApiResponse;
          this.branchesCount.emit(this.branches.length);
          this.isLoading = false;
        }, (errorData: ApiError) => {
          this.errorData = errorData;
          this.isLoading = false;
        }));
  }

  ngOnDestroy(): void {
    unsubscribeAll(this.subscriptions);
  }

}
