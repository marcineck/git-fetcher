import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-repos-detail',
  templateUrl: './repos-detail.component.html',
  encapsulation: ViewEncapsulation.None
  // styleUrls: ['./repos-detail.component.scss']
})
export class ReposDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
