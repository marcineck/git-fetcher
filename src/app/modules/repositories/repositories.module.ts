import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RepositoriesRoutingModule } from './repositories-routing.module';
import { ReposListComponent } from './repos-list/repos-list.component';
import { ReposDetailComponent } from './repos-detail/repos-detail.component';
import {PreloaderModule} from '../shared/preloader/preloader.module';
import {PreloaderComponent} from '../shared/preloader/preloader.component';
import { ReposBranchListComponent } from './repos-branch-list/repos-branch-list.component';


@NgModule({
  declarations: [ReposListComponent, ReposDetailComponent, ReposBranchListComponent],
  imports: [
    CommonModule,
    RepositoriesRoutingModule,
    PreloaderModule
  ],
  exports: [PreloaderComponent]
})
export class RepositoriesModule { }
