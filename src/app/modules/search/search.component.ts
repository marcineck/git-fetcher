import {Component, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  encapsulation: ViewEncapsulation.None
  // styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  searchForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router) {
    this.searchForm = this.fb.group({
      username: this.fb.control('', [Validators.required])
    });
  }

  submitForm() {
    return this.router.navigate(['/repo', this.searchForm.get('username').value]);
  }
}
