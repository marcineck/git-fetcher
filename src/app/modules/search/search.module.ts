import { SharedFormsModule } from './../shared/forms/sharedForms.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';

@NgModule({
  declarations: [SearchComponent],
  imports: [CommonModule, SharedFormsModule],
  exports: [SearchComponent]
})
export class SearchModule {}
